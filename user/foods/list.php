<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/foods/list.php";

$action = get('action');
$id = get('id');

$result = null;
switch ($action) {
    case 'add':
        $char = DB::row("SELECT * FROM `cart` WHERE `user_id`='{$user_id}' AND `food_id`='{$id}'");
        if (empty($char)) {
            $result = DB::insert('cart', [
                'user_id' => $user_id,
                'food_id' => $id,
                'amount' => 1
            ]);
        } else {
            $result = DB::update('cart', [
                'amount' => intval($char['amount']) + 1
            ], "`cart_id`='{$char['cart_id']}'");
        }
        break;
}

if ($action) {
    if ($result) {
        setAlert('success', "เพิ่มอาหารในรถเข็นสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มอาหารในรถเข็นได้");
    }
    redirect($page_path);
}

$items = DB::result("SELECT * FROM `foods`");
ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ภาพอาหาร</th>
            <th>ชื่ออาหาร</th>
            <th>ราคาอาหาร</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['food_id'] ?></td>
                <td>
                    <img src="<?= url($item['food_img']) ?>" alt="" style="
                        max-height: 8rem;
                    ">
                </td>
                <td><?= $item['food_name'] ?></td>
                <td><?= $item['food_price'] ?></td>
                <td>
                    <a href="?action=add&id=<?= $item['food_id'] ?>">
                        เพิ่มอาหารในรถเข็น
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการอาหาร';
require ROOT . '/user/layout.php';
