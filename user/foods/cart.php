<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/foods/cart.php";

$action = get('action');
$id = get('id');
$value = get('value');

$result = null;
switch ($action) {
    case 'update':
        $result = DB::update('cart', [
            'amount' => $value
        ], "`cart_id`='{$id}'");
        break;

    case 'delete':
        $result = DB::delete('cart', "`cart_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

$items = DB::result("SELECT 
* ,
(`foods`.`food_price` * `cart`.`amount`) AS sum_price    
FROM `cart`
INNER JOIN `foods` ON `foods`.`food_id`=`cart`.`food_id`
WHERE `cart`.`user_id`='{$user_id}'");
$total_price = 0;
foreach ($items as $item) {
    $total_price += intval($item['sum_price']);
}

if ($_POST) {
    DB::insert('order', [
        'user_id' => $user_id,
        'total_price' => $total_price,
        'get_food_time' => post('get_food_time'),
        'created_at' => date(DATE_SQL),
        'status' => '0'
    ]);
    $order_id = DB::insert_id();

    $order_item_data = [];

    foreach ($items as $item) {
        $order_item_data[] = [
            'order_id' => $order_id,
            'food_id' => $item['food_id'],
            'amount' => $item['amount'],
            'price' => $item['food_price'],
            'total' => $item['sum_price']
        ];

        DB::delete('cart', "`cart_id`='{$item['cart_id']}'");
    }

    DB::insert_multi('order_item', $order_item_data);

    setAlert('success', "สั่งอาหารสำเร็จเรียบร้อย");
    redirect($page_path);
}
ob_start();
?>
<?= showAlert() ?>
<h3>รายการอาหารในรถเข็น</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ภาพอาหาร</th>
            <th>ชื่ออาหาร</th>
            <th>ราคาอาหาร</th>
            <th>จำนวน</th>
            <th>รวม</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['food_id'] ?></td>
                <td>
                    <img src="<?= url($item['food_img']) ?>" alt="" style="
                        max-height: 8rem;
                    ">
                </td>
                <td><?= $item['food_name'] ?></td>
                <td><?= $item['food_price'] ?></td>
                <td><?= $item['amount'] ?></td>
                <td><?= $item['sum_price'] ?></td>
                <td>
                    <a href="?action=update&id=<?= $item['cart_id'] ?>&value=<?= intval($item['amount']) + 1 ?>">
                        เพิ่มจำนวน
                    </a>
                    <?php if ($item['amount'] !== '1') : ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="?action=update&id=<?= $item['cart_id'] ?>&value=<?= intval($item['amount']) - 1 ?>">
                            ลดจำนวน
                        </a>
                    <?php endif; ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['cart_id'] ?>">
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if (!empty($items)) : ?>
            <tr>
                <td colspan="5">ราคารวม</td>
                <td><?= $total_price ?></td>
                <td>บาท</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>
<?php if (!empty($items)) : ?>
<form method="post">
    <label for="get_food_time">เวลารับอาหาร</label>
    <input type="time" name="get_food_time" id="get_food_time" required>
    <button type="submit">สั่งอาหาร</button>
</form>
<?php endif; ?>
<?php
$layout_page = ob_get_clean();
$page_name = 'รถเข็น';
require ROOT . '/user/layout.php';
