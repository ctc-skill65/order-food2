<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/foods/list.php";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('foods', "`food_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if (post('food_name')) {
    $food_img = upload('food_img');
    if ($food_img == false) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพได้");
        redirect($page_path);
    }

    $result = DB::insert('foods', [
        'food_img' => $food_img,
        'food_name' => post('food_name'),
        'food_price' => post('food_price')
    ]);

    if ($result) {
        setAlert('success', "เพิ่มอาหารสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มอาหารได้");
    }
    redirect($page_path);
}

$items = DB::result("SELECT * FROM `foods`");
ob_start();
?>
<?= showAlert() ?>
<h3>เพิ่มอาหาร</h3>
<form method="post" enctype="multipart/form-data">
    <label for="food_name">ชื่ออาหาร</label>
    <input type="text" name="food_name" id="food_name" required>
    <br>   

    <label for="food_price">ราคาอาหาร</label>
    <input type="number" name="food_price" id="food_price" required>
    <br>

    <label for="food_img">ภาพอาหาร</label>
    <input type="file" name="food_img" id="food_img" accept="image/*" required>
    <br>

    <button type="submit">บันทึก</button>
</form>

<h3>รายการอาหาร</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ภาพอาหาร</th>
            <th>ชื่ออาหาร</th>
            <th>ราคาอาหาร</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['food_id'] ?></td>
                <td>
                    <img src="<?= url($item['food_img']) ?>" alt="" style="
                        max-height: 8rem;
                    ">
                </td>
                <td><?= $item['food_name'] ?></td>
                <td><?= $item['food_price'] ?></td>
                <td>
                    <a href="<?= url("/admin/foods/edit.php?id={$item['food_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['food_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบ {$item['food_name']} หรือไม่") ?>
                    >
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการอาหาร';
require ROOT . '/admin/layout.php';
