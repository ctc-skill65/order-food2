<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$food_id = get('id');
$page_path = "/admin/foods/edit.php?id={$food_id}";

if (post('food_name')) {
    if (!empty($_FILES['food_img']['name'])) {
        $food_img = upload('food_img');
        if ($food_img == false) {
            setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพได้");
            redirect($page_path);
        }

        DB::update('foods', [
            'food_img' => $food_img
        ], "`food_id`='{$food_id}'");
    }

    $result = DB::update('foods', [
        'food_name' => post('food_name'),
        'food_price' => post('food_price')
    ], "`food_id`='{$food_id}'");

    if ($result) {
        setAlert('success', "แก้ไขอาหารสำเร็จเรียบร้อย");
        redirect("/admin/foods/list.php");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขอาหารได้");
    }
    redirect($page_path);
}

$data = DB::row("SELECT * FROM `foods` WHERE `food_id`='{$food_id}'");
ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="food_img">
        <img src="<?= url($data['food_img']) ?>" alt="" style="
            max-height: 14rem;
        ">
    </label>
    <br>

    <label for="food_name">ชื่ออาหาร</label>
    <input type="text" name="food_name" id="food_name" value="<?= $data['food_name'] ?>" required>
    <br>   

    <label for="food_price">ราคาอาหาร</label>
    <input type="number" name="food_price" id="food_price" value="<?= $data['food_price'] ?>" required>
    <br>

    <label for="food_img">ภาพอาหาร</label>
    <input type="file" name="food_img" id="food_img" accept="image/*">
    <br>

    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขอาหาร';
require ROOT . '/admin/layout.php';
